import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import Main from './Main.vue'
import User from './User.vue'

Vue.use(Router)

var router = new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path:'/User/:id',
      name: 'User',
      component: User
    }
  ]
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
