(function (angular) {
    'use strict';

    angular
        .module('app.factories')
        .factory('movieService', Definition);

    Definition.$inject = ['$q', '$http'];

    function Definition($q, $http) {
        var apiKey = '31690548';

        return {
            getPage: function (idx, term) {
                idx = idx || 1;
                return $q(function (resolve) {
                    $http
                        .get(`https://www.omdbapi.com/?apiKey=${apiKey}&s=${term}&page=${idx}`)
                        .then(function (response) {
                            resolve(response.data.Search.map(function (item) {
                                return {
                                    title: item.Title,
                                    year: item.Year,
                                    poster: item.Poster
                                };
                            }));
                        });
                });
            }
        };
    };
})(angular)