(function (angular) {
    'use strict';

    angular
        .module('app.factories')
        .factory('loginService', Definition);

    Definition.$inject = ['$q'];

    function Definition($q) {
        return {
            login: function (username, password) {
                return $q(function (resolve, reject) {
                    if (username != 'jcosio') {
                        reject();
                    };
                    if (password != '1234') {
                        reject();
                    };
                    resolve();
                });
            }
        };
    };
})(angular)