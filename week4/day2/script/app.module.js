(function(angular){
    'use strict';

    angular
        .module('app', [
            'app.controllers',
            'app.directives',
            'app.filters',
            'app.factories'
        ]);
})(angular);

// (function(angular) {
//     'use strict';
// })(angular)

(function(angular) {
    'use strict';

    angular.module('app.controllers', []);
})(angular);

(function(angular) {
    'use strict';

    angular.module('app.directives', []);
})(angular);

(function(angular) {
    'use strict';

    angular.module('app.filters', []);
})(angular);

(function(angular) {
    'use strict';

    angular.module('app.factories', [])
})(angular);

// john papa angular style guide