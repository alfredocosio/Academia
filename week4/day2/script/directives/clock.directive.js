(function (angular) {
    'use strict';

    angular
        .module('app.directives')
        .directive('clockDirective', Definition);

    // Definition.$inject = [];

    function Definition() {
        return {
            controller: 'clockController',
            controllerAs: 'ctrl',
            restrict: 'E',
            scope: {},
            templateUrl: 'script/templates/clock.html'
        }
    }
})(angular)