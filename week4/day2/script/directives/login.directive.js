(function (angular) {
    'use strict';

    angular
        .module('app.directives')
        .directive('userLogin', Definition);

    // Definition.$inject = [];

    function Definition() {
        return {
            controller: 'loginController',
            controllerAs: 'lgc',
            restrict: 'E',
            templateUrl: 'script/templates/login.html'
        }
    };
})(angular)