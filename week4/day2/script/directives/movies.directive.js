(function (angular) {
    'use strict';

    angular
        .module('app.directives')
        .directive('movieSearch', Definition);

    // Definition.$inject = [];

    function Definition() {
        return {
            controller: 'movieController',
            controllerAs: 'mc',
            restrict: 'E',
            scope: {},
            templateUrl: 'script/templates/movies.html'
        }
    };
})(angular)