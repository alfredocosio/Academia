(function(angular) {
    'use strict';

    angular
        .module('app.controllers')
        .controller('clockController', Definition);
    
    Definition.$inject = ['$interval', '$filter'];
    // hacer filtro padding que recibe cuantos digitos quieres
    function Definition($interval, $filter) {
        var jc = this;
        jc.userName = 'Alfredo Cosio';
        jc._seconds = 0; jc._minutes = 0; jc._hours = 0; jc._milisec = 0;
        jc.milisec = $filter('twoDigit')(jc._milisec);
        jc.seconds = $filter('twoDigit')(jc._seconds);
        jc.minutes = $filter('twoDigit')(jc._minutes);
        jc.hours = $filter('twoDigit')(jc._hours);
        var ShowTime = function () {
            if (jc._milisec >= 100) {
                jc._seconds++;
                jc._milisec -= 100;
            }
            if (jc._seconds >= 60) {
                jc._minutes++;
                jc._seconds -= 60;
            }
            if (jc._minutes > 59) {
                jc._hours++;
                jc._minutes -= 60;
            }
            if (jc._hours > 23) {
                jc.days++;
                jc._hours -= 24;
            }
            if (jc.weeks > 3) {
                jc.months++;
                jc.weeks -= 4;
            }
            if (jc.months > 11) {
                jc.years++;
                jc.months -= 12;
            }
            jc.milisec = $filter('twoDigit')(jc._milisec);
            jc.seconds = $filter('twoDigit')(jc._seconds);
            jc.minutes = $filter('twoDigit')(jc._minutes);
            jc.hours = $filter('twoDigit')(jc._hours);
        };
        
        var TickTime = function () {
            if (!jc.clockId) {
                jc.clockId = $interval(function () {
                    jc._milisec += 1
                    jc.showTime();
                }.bind(jc), 10);
                jc.isRunning = true;
            }
        };
        var PauseTime = function () {
            $interval.cancel(jc.clockId);
            jc.clockId = 0;
        };
        var ResetTime = function () {
            jc._milisec = 0; jc._seconds = 0; jc._minutes = 0; jc._hours = 0;
            jc.pauseTime();
            jc.showTime();
        };

        jc.showTime = ShowTime;
        jc.tickTime = TickTime;
        jc.pauseTime = PauseTime;
        jc.resetTime = ResetTime;
    }
})(angular)