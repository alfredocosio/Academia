(function (angular) {
    'use strict';

    angular
        .module('app.controllers')
        .controller('loginController', Definition);

    Definition.$inject = ['loginService', '$rootScope'];

    function Definition(loginService, $rootScope) {
        var lgc = this;
        lgc.show = true;
        var Login = function (username) {
            console.log(username);
            loginService.login(username, '1234')
                .then(function () {
                    console.log('login success!');
                    $rootScope.$broadcast('login', true);
                    lgc.show = false;
                })
                .catch(function () {
                    console.log('fail');
                });
        };
        lgc.login = Login;
    }
})(angular)