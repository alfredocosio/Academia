(function (angular) {
    'use strict';

    angular
        .module('app.controllers')
        .controller('movieController', Definition);

    Definition.$inject = ['movieService', '$rootScope'];

    function Definition(movieService, $rootScope) {
        var mc = this;
        $rootScope.$on('login', function (event, args) {
            mc.logged = args.value;
        })
        var Search = function (text) {
            console.log('Searching ' + text);
            movieService.getPage(1, text).then(function (movies) {
                mc.movies = movies;
            });
        };
        mc.search = Search;
    }
})(angular)