(function (angular) {
    'use strict';

    angular
        .module('app.filters')
        .filter('twoDigit', Definition);

    // Definition.$inject = [];

    function Definition() {
        return function (value) {
            return ('00' + value).slice(-2);
        }
    }
})(angular)