'use strict';

angular.module('myApp.clock', ['ngRoute'])
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/clock', {
        templateUrl: 'clock.html',
        controller: 'ClockCtrl',
        controllerAs: 'ctrl'
    });
}])
.controller('ClockCtrl', [function () {
    this.userName = 'Alfredo Cosio';
    this.seconds = 0; ctrl.minutes = 0; ctrl.hours = 0;
    // this.showTime = function () {
    //     if (ctrl.seconds >= 60) {
    //         ctrl.minutes++;
    //         ctrl.seconds -= 60;
    //     }
    //     if (ctrl.minutes > 59) {
    //         ctrl.hours++;
    //         ctrl.minutes -= 60;
    //     }
    //     if (ctrl.hours > 23) {
    //         ctrl.days++;
    //         ctrl.hours -= 24;
    //     }
    //     if (ctrl.weeks > 3) {
    //         ctrl.months++;
    //         ctrl.weeks -= 4;
    //     }
    //     if (ctrl.months > 11) {
    //         ctrl.years++;
    //         ctrl.months -= 12;
    //     }
    // };
    // this.tickTime = function () {
    //     if (!ctrl.clockId) {
    //         ctrl.clockId = setInterval(function () {
    //             ctrl.seconds += 1
    //             ctrl.methods.showTime();
    //         }.bind(ctrl), 1000);
    //         ctrl.isRunning = true;
    //     }
    // };
    // this.pauseTime = function () {
    //     clearInterval(ctrl.clockId);
    //     ctrl.clockId = 0;
    // };
    // this.resetTime = function () {
    //     ctrl.methods.pauseTime();
    //     ctrl.methods.showTime();
    // };
}]);