
// var app = new Module({selector:'app', components: ['clock', 'greet']});

var ruter = new Router({
    '': {
        template: `
        <section class="component" data-component="clock"></section>
        <section class="component" data-component="greet"></section>
        <section class="component" data-component="contact"></section>`,
        components: ['clock', 'greet', 'contact']
    },
    'clock': {
        template: `
        <section class="component" data-component="clock"></section>
        <section class="component" data-component="clock"></section>`,
        components: ['clock']
    },
    'greet': {
        template: `
        <section class="component" data-component="greet"></section>`,
        components: ['greet']
    },
    'contact': {
        template: `
        <section class="component" data-component="contact"></section>`,
        components: ['contact']
    }
})