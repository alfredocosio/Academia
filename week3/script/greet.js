var Greet = class Greet extends Component {
    constructor(selectedNode) {
        super({
            node: selectedNode,
            template: `<h2 class="moduleHeader">Greet</h2>
<form action="#">
        <label for="firstName">My name is:</label><input type="text" id="firtName" name="name" data-value="name" id="firstName">
        <label for="lastName">My last name is:</label><input type="text" id="lastName" name="lastName" id="lastName" data-value="last">
        <p>Hello, my name is <span data-value="name"></span> <span data-value="last"></span></p>
        <label for="yesNoChk" data-value="yesNo"></label>
        <input type="checkbox" id="yesNoChk" data-value="yesNo" data-event-click="yesNo">
        <button type="submit">Enter</button>
</form>`,
            data: { name: 'Alfredo', last: '', yesNo: "You haven't checked the checkbox.", clickCheck: false },
            methods: {
                yesNo() {
                    if (!this.data.clickCheck) {
                        this.data.yesNo = 'You have checked the checkbox.';
                    }
                    else {
                        this.data.yesNo = 'You have unchecked the checkbox.';
                    }
                    this.data.clickCheck = !this.data.clickCheck;
                }
            }
        });
    }
}