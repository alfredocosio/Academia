class Router {
    constructor(routes = {}) {
        try {
            this.routes = routes;
            this.containter = document.querySelector('[data-router]');
            this.mount();
        } catch (error) {
            console.log(error);
        }
    }

    mount() {
        addEventListener('hashchange', () => {
            this.hashChanged(window.location.hash);
        })
    }

    hashChanged(hash = '') {
        hash = hash.slice(1);
        this.containter.innerHTML = '';
        if (this.routes.hasOwnProperty(hash)) {
            this.containter.innerHTML = '<section class="mainApp" data-module="app">' + this.routes[hash].template + '</section>';
            var ap = new Module({ selector: 'app', components: this.routes[hash].components });
        }
        else {
            this.containter.innerHTML = '<h1>404 NOT FOUND<h1>'
        }
    }
}