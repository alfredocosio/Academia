{/* <span class="timeDisplay" data-value="years"></span>/<span class="timeDisplay" data-value="months"></span>/<span class="timeDisplay" data-value="weeks"></span>/<span class="timeDisplay" data-value="days"></span> - <span class="timeDisplay" data-value="hours"></span>:<span class="timeDisplay" data-value="minutes"></span>:<span class="timeDisplay" data-value="seconds"></span> */ }
var Clock = class Clock extends Component {
    constructor(selectedNode) {
        super({
            node: selectedNode,
            template: `<h2 class="moduleHeader">Clock</h2>
<section class="display">
<span class="timeDisplay" data-value="hours"></span>:
<span class="timeDisplay" data-value="minutes"></span>:
<span class="timeDisplay" data-value="seconds"></span>
</section>
<section class="controls">
<button class="buttonControls" data-event-click="tickTime">Start</button>
<button class="buttonControls" data-event-click="pauseTime">Pause</button>
<button class="buttonControls" data-event-click="resetTime">Reset</button>
</section>`,
            data: { hours: 0, minutes: 0, seconds: 0 },
            methods: {
                showTime() {
                    if (this.data.seconds >= 60) {
                        this.data.minutes++;
                        this.data.seconds -= 60;
                    }
                    if (this.data.minutes > 59) {
                        this.data.hours++;
                        this.data.minutes -= 60;
                    }
                    if (this.data.hours > 23) {
                        this.data.days++;
                        this.data.hours -= 24;
                    }
                    if (this.data.weeks > 3) {
                        this.data.months++;
                        this.data.weeks -= 4;
                    }
                    if (this.data.months > 11) {
                        this.data.years++;
                        this.data.months -= 12;
                    }
                },
                tickTime() {
                    if (!this.clockId) {
                        this.clockId = setInterval(function () {
                            this.data.seconds += 1
                            this.methods.showTime();
                        }.bind(this), 1000);
                        this.isRunning = true;
                    }
                },
                pauseTime() {
                    clearInterval(this.clockId);
                    this.clockId = 0;
                },
                resetTime() {
                    this.methods.pauseTime();
                    for (const key in this._data) {
                        this.data[key] = 0;
                    }
                    this.methods.showTime();
                }
            }
        });
    }
}