var Contact = class Contact extends Component {
    constructor(selectedNode) {
        super({
            node: selectedNode,
            template: `<h2>Contact Us</h2>
<form action="">
<label for="emailIn">Email:</label><input type="email" name="email" id="emailIn" data-value="email">
<label for="messageIn">Message:</label><input type="text" name="message" id="messageIn" data-value="message">
<button type="submit">Submit</button>
</form>`,
            data: {email: '', message:''},
            methods: {}
        });
    }
}