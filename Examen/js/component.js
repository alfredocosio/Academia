// La clase componente es una forma de encapsular, abstraer y estandarizar el como construiremos
// pequeños programas en javascript. De este modo podemos escribir todo tipo de componentes, de modo
// que su implementacion sea relativamente sencilla, solo hace falta que escribamos la estructura
// del componente (que es lo que la clase recibe como parametro en su constuctor), y cada que
// necesitemos el componente, solo hace falta incluir el archivo js, y agregar el atributo
// correspondiente al tag contenedor donde queremos que se renderize el codigo.
// Esto tambien, mantiene el documento HTML libre de logica, y con un aspecto limpio y legible.
class Component {
    // El constructor recibe un "objeto" que debe contener:
    // - El 'selector', que es utilizado para encontrar que parte del codigo donde se debe renderear nuestro
    // componente. i.e. 'clock', 'greet', etc.
    // - El templete, que es el string que contiene la estructura de nuestro componente. Incluye todo
    // span, label, boton, etc
    // - 'Data' que contiene las "variables" que se van a estar usando y/o modificando en nuestro
    // componente.
    // - 'Methods', aqui incluimos la logica que nuestro componente utiliza para saber como responder
    // a los diferentes eventos que se registren, se procesa la informacion y se cambian las
    // variables en 'Data' para que luego nuestro componente renderize los cambios en tiempo real
    constructor(structure = {
        selector: '',
        template: '',
        data: {},
        methods: {}
    }) {
        try {
            this.selector = `[data-component="${structure.selector}"]`;
            this.container = document.querySelector(this.selector);
            this.template = structure.template;
            this.methods = structure.methods;
            this.data = this.buildData(structure.data);
            this.mount();
        } catch (e) {
            console.log(e)
        }
    }
    // En este metodo de la clase Component, se inicializan las variables de 'Data'. Se mapean a
    // un Objeto que tiene como "propiedades" nuestros valores de Data, y se establecen las funciones
    // de 'get y 'set', que se ejecutan cada vez que eremos cambiar u obtener los valores de estas
    // propiedades, respectivamente. Es esta funcion de 'set' la que actualiza nuestro DOM, cada que
    // una de nuestrar variables en Data cambia.
    buildData(data = {}) {
        var computedInitalData = {}
        this._data = data;
        for (const propertyName in data) {
            if (data.hasOwnProperty(propertyName)) {
                Object.defineProperty(computedInitalData, propertyName, {
                    set: function (x) {
                        this._data[propertyName] = x;
                        var toChange = this.container.querySelectorAll(`[data-value="${propertyName}"]`);
                        if (toChange) {
                            this.render(toChange, data[propertyName])
                        }
                    }.bind(this),
                    get: () => (this._data[propertyName])
                });
            }
        }
        return computedInitalData;
    }
    // Render recibe el resultado de un querySelector(All), y un 'contenido' en HTML para reemplazar
    // todas las instancias resultado del selector, con el HTML de 'contenido'.
    // Este metodo es llamado por todos los 'setters' de todas las propiedades en nuestro
    // component.Data.
    render(element = document.querySelector(this.selector), content = this.template) {
        if (Node.prototype.isPrototypeOf(element)) {
            if (element.tagName == "INPUT") {
                element.value = content;
            } else {
                element.innerHTML = content;
            }

        }
        if (NodeList.prototype.isPrototypeOf(element)) {
            element.forEach((child) => {
                this.render(child, content)
            });
        }
    }
    // Nuestra función Mount, es la que se encarga de 'montar' nuestro componente (como su 
    // nombre lo indica). Al montar la aplicación, primero se busca con el selector base
    // donde tiene que renderearze el templete, luego se buscan todas las referencias a nuestras
    // variables en Data ([data-value="{key}"]) para irlas ligando a nuestros atributos
    // correspondientes en component.Data. Así, cada que cambien nuestros elementos input del DOM
    // ligados a nuetras variables, las variables son actualizadas con la ultima informacion.
    // Y estas a su vez, al ser actualizadas, con la funcion 'set' hacen que el DOM refleje la
    // informacion en los lugares correspondientes.
    mount() {
        this.render(this.container, this.template);
        for (const key in this._data) {
            if (this.data.hasOwnProperty(key)) {
                var toChange = this.container.querySelectorAll(`[data-value="${key}"]`);
                if (toChange) {
                    this.render(toChange, this.data[key]);
                    toChange.forEach(element => {
                        if (element.tagName == 'INPUT') {
                                element.addEventListener('input', event=> {
                                this.data[key] = event.target.value;
                                })
                            if (element.getAttribute('type') == 'checkbox') {
                                    element.addEventListener('click', (click) => {
                                        this.data[key] = click.target.checked
                                        })
                                    }
                        }
                    });
                }
            }
        }
        const events = ['click']
        const DOMElements = events.map(event => {
            return {
                event: event,
                targets: this.container.querySelectorAll(`[data-event-${event}]`)
            };
        });
        for (const method in this.methods) {
            if (this.methods.hasOwnProperty(method)) {
                this.methods[method] = this.methods[method].bind(this);
            }
        }
        DOMElements.forEach(element => {
            element.targets.forEach(target => {
                target.addEventListener(element.event, this.methods[target.getAttribute('data-event-' + element.event)]);
            });
        });
    }
}