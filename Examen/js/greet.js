// Otra clase que extiende Componente.
// Se pasa el selector, templete y datos. Metodos esta vacio, porque no existe
// ninguna logica que procesar con la informacion recibida, simplemente deben
// reflejarse los cambios en el DOM.
class Greet extends Component {
    constructor() {
        super({     // Se hace la llamada al constructor del "padre" de nuestra clase: Component.
            selector: "greet",
            template: `
                    <article class="greet">
                        <input type="text" data-value="name">
                        <p>Hello my name is <span data-value="name"></span></p>
                        <input type="text" data-value="lastname">
                        <p>And my name is <span data-value="lastname"></span></p>
                        <input type="checkbox" data-value="flag">
                        <span data-value="flag"></span>
                    </article>`,
            data: {name:"Alan",lastname:"",flag:""},
            methods: {}
        });
    }
};
