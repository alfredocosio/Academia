// La clase Clock que extiende el comportamiento de Component. Un ejemplo de como se pueden utilizar
// los componentes para hacer un pequeño programa que corre un cronometro simple.
// El selector es el mismo nombre de la clase, el templete lleva la escructura basica de nuestro
// cronometro y Data lleva el conteo basico de segundos, minutos y horas.
// Los metodos de Clock son los que se encargan de poner el intervalo de nuestro segundero,
// actualizan los segundos, los minutos y horas conforme pasa el tiempo. Tambien pueden pausar
// o detener el cronometro por completo, y esto reinicia los tiempos.
// Notese que no se incluye ninguna logica para actualizar el DOM. Al actualizar las variables
// se renderizan los cambios automaticamente en el DOM.
class Clock extends Component {
    constructor() {
        super({     // Esto hace una llamada al constructor del "padre" de nuestra clase: Component.
            selector: "clock",
            template: `
                    <article class="reloj">
                        <section class="display">
                            <span class="hours" data-value="hours">0</span>
                            <span class="minutes" data-value="minutes">0</span>
                            <span class="seconds" data-value="seconds">0</span>
                        </section>
                        <section class="controls">
                            <button data-event-click="startClock">Start</button>
                            <button data-event-click="pauseClock">Pause</button>
                            <button data-event-click="stopClock">Stop</button>
                        </section>
                    </article>`,
            data: {
                seconds: 0,
                minutes: 0,
                hours: 0
            },
            methods: {
                updateClock() {
                    if (this.data.seconds < 59) {
                        this.data.seconds++;
                    } else {
                        this.data.seconds = 0;
                        this.data.minutes++;
                    }
                    if (this.data.minutes >= 60) {
                        this.data.minutes = 0;
                        this.data.hours++;
                    }
                },
                startClock() {
                    if (!this.data.clockId) {
                        this.data.clockId = setInterval(function () {
                            this.methods.updateClock();
                        }.bind(this), 100);
                    }

                },
                pauseClock() {
                    clearInterval(this.data.clockId)
                    this.data.clockId = 0;
                },
                stopClock() {
                    clearInterval(this.data.clockId)
                    this.data.seconds = 0;
                    this.data.minutes = 0;
                    this.data.hours = 0;
                    this.data.clockId = 0;
                }
            }
        });
    }
};
