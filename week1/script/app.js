var seconds = 0.0;
var chrmInterval;
var isRunning = false;
var mainContainer = document.querySelector('[data-module="app"]');
var template = `<article>
<section class="display">
<span class="seconds" id="mainWatch" data-value="seconds">${seconds}</span>
</section>
<section class="controls">
<button class="buttonControls" onclick="tickTime()">Start</button>
<button class="buttonControls" onclick="pauseTime()">Pause</button>
<button class="buttonControls" onclick="resetTime()">Reset</button>
</section>
</article>`;
mainContainer.innerHTML=template;
var secondContainer = document.querySelector('[data-value="seconds"]');

function showTime() {
    secondContainer.textContent = seconds.toFixed(2);
}

function tickTime() {
    if(!isRunning) {
        chrmInterval = setInterval(function(){
            seconds+= .01
            showTime();
        },10);
        isRunning = true;
    }
};

function pauseTime() {
    clearInterval(chrmInterval);
    isRunning = false;
};

function resetTime() {
    pauseTime();
    seconds = 0;
    showTime();
}


// class Auto{
//     constructor(nombre){
//         this.nombre = nombre;
//     }

//     run(){
//         console.log('Running ' + this.nombre + '');
//     }
// }

// var TeslaX = new Auto('Tesla Model X');
// TeslaX.run();

// const pi = {value: 3.141593, square: 0};
// pi.square = 9.8696044;

// console.log(pi);
// var {square}=pi;
// console.log(square);