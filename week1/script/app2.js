(function(){
    var scope = {
        container : document.querySelector('[data-module="app"]'),
        init(){
            this.container.innerHTML = this.template;
            scope.render();
        },
        render(){
            this.mount();
        },
        mount(){
            for(const key in this.data){
                var tag = this.container.querySelectorAll('[data-value="'+key+'"]');
                if(tag){
                    tag.forEach(element => {
                        element.innerHTML = this.data[key];
                    });
                }
            };
            for(const key in this.methods){
                var tag = this.container.querySelectorAll('[data-event-click="'+key+'"]');
                if(tag){
                    tag.forEach(element => {
                        element.addEventListener('click', function(){
                            this.methods[key]();
                        });
                    });
                }
            };
            // timeEnt.forEach(key => {
            //     console.log(docu);
            //     var 
            //     console.log(docu2);
            //     docu.innerHTML=docu2;
            //     console.log(docu);
            //     // document.querySelector('[data-value="'+key+'"]').innerHTML=scope.data[key];
            // });
            // document.querySelector('[data-value="seconds"]').innerHTML=this.data.seconds.toFixed(2);
            // document.querySelector('[data-value="minutes"]').innerHTML=this.data.minutes;
        },
        template: `<article>
        <section class="display">
            <span class="timeDisplay" data-value="years"></span>/<span class="timeDisplay" data-value="months"></span>/<span class="timeDisplay" data-value="weeks"></span>/<span class="timeDisplay" data-value="days"></span> - <span class="timeDisplay" data-value="hours"></span>:<span class="timeDisplay" data-value="minutes"></span>:<span class="timeDisplay" data-value="seconds"></span>
        </section>
        <section class="controls">
        <button class="buttonControls" data-event-click="tickTime">Start</button>
        <button class="buttonControls" data-event-click="pauseTime">Pause</button>
        <button class="buttonControls" data-event-click="resetTime">Reset</button>
        </section>
        </article>`,
        data:{seconds:0, minutes:0, hours:0, days:0, weeks:0, months:0, years:0},
        methods:{
            showTime() {
                if(scope.data.seconds >= 60){
                    scope.data.minutes++;
                    scope.data.seconds-=60;
                }
                if(scope.data.minutes > 59){
                    scope.data.hours++;
                    scope.data.minutes-=60;
                }
                if(scope.data.hours > 23){
                    scope.data.days++;
                    scope.data.hours-=24;
                }
                if(scope.data.weeks > 3){
                    scope.data.months++;
                    scope.data.weeks-=4;
                }
                if(scope.data.months > 11){
                    scope.data.years++;
                    scope.data.months-=12;
                }
                scope.render();
            },
            tickTime() {
                if(!scope.clockId) {
                    scope.clockId = setInterval(function(){
                        scope.data.seconds+= 10
                        showTime();
                    },.1);
                    scope.isRunning = true;
                }
            },        
            pauseTime() {
                clearInterval(scope.clockId);
                scope.clockId = 0;
            },        
            resetTime() {
                pauseTime();
                for(const key in scope.data){
                    scope.data[key] = 0;
                };
                showTime();
            }
        }
    };
    scope.init();
})();