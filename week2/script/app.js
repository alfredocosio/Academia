
// var app = new Module({selector:'app', components: ['clock', 'greet']});

var ruter = new Router({
    '': {
        template: `
        <header><h1>Home</h1></header>
        <article data-component="clock"></article>
        <article data-component="clock"></article>
        <article data-component="greet"></article>`,
        components: ['clock', 'greet']
    },
    'clock': {
        template: `
        <header><h1>Clock</h1></header>
        <article data-component="clock"></article>
        <article data-component="clock"></article>`,
        components: ['clock']
    },
    'greet': {
        template: `
        <header><h1>Greet</h1></header>
        <article data-component="greet"></article>
        <article data-component="greet"></article>`,
        components: ['greet']
    }
})