var Greet = class Greet extends Component {
    constructor(selectedNode) {
        super({
            node: selectedNode,
            template: `<article class="greet">
    <input type="text" name="name" data-value="name" id="firstName">
    <p><span>Hello, my name is </span><span data-value="name"></span></p>
    <input type="text" name="last" id="lastName" data-value="last">
    <p><span>And my last name is </span><span data-value="last"></span></p>
    <input type="checkbox" data-value="yesNo">
    <span data-value="yesNo"></span>
</article>`,
            data: { name: 'Alfredo', last: '', yesNo: 'false' },
            methods: {}
        });
    }
}