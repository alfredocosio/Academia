class Router {
    constructor(routes = {}) {
        try {
            this.routes = routes;
            this.containter = document.querySelector('[data-router]');
            this.mount();
        } catch (error) {
            console.log(error);
        }
    }

    mount() {
        addEventListener('hashchange', () => {
            this.hashChanged(window.location.hash);
        })
    }

    hashChanged(hash = '') {
        hash = hash.slice(1);
        this.containter.innerHTML = '';
        if (this.routes.hasOwnProperty(hash)) {
            this.containter.innerHTML = '<div data-module="app">' + this.routes[hash].template + '</div>';
            var ap = new Module({ selector: 'app', components: this.routes[hash].components });
        }
        else {
            // turn this into another component
            location.replace("Error.html");
            var errorCode = document.querySelector('#errorCode');
            errorCode.textContent = errorCode.textContent + ' 404';
            var errorDescription = document.querySelector('#errorDescription');
            errorDescription.textContent = 'Not found';
        }
    }
}