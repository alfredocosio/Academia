class Module {
    constructor(structure = {
        selector: '',
        components: []
    }) {
        try {
            this.selector = structure.selector
            this.container = document.querySelector('[data-module="' + this.selector + '"]');
            this.components = structure.components;
            this.mount();
        } catch (error) {
            console.log(error);
        }
    }

    mount() {
        var allElem = this.container.querySelectorAll('[data-component]');
        allElem.forEach((elem) => {
            var comp = elem.getAttribute('data-component');
            if (this.components.includes(comp)) {
                comp = comp.charAt(0).toUpperCase() + comp.slice(1);
                var i = new window[comp](elem);
            }
        });
    }
}