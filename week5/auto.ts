class Auto {
    insurance:string;
    year: number|string;
    constructor(insurance:string, year:number|string) {
        this.insurance = insurance;
        this.year = year;
    }
}

function Stereo(argument) {
    argument.stereo = 'Sony Ax400';
}

@Stereo
class Ferrari extends Auto {
    constructor(ins:string, yr:number|string) {
        super(ins,yr);
        console.log("Construyendo carro Ferrari"+this)
    }
}

var carr1 = new Ferrari("Liberty Mutual", 2017);