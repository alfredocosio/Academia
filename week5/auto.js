var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
class Auto {
    constructor(insurance, year) {
        this.insurance = insurance;
        this.year = year;
    }
}
function Stereo(argument) {
    argument.stereo = 'Sony Ax400';
}
let Ferrari = class Ferrari extends Auto {
    constructor(ins, yr) {
        super(ins, yr);
        console.log("Construyendo carro Ferrari" + this);
    }
};
Ferrari = __decorate([
    Stereo
], Ferrari);
var carr1 = new Ferrari("Liberty Mutual", 2017);
