import { Component } from '@angular/core'
import { Album } from './models/Album';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  albums: Album[] = [
    {
      group: 'Beatles',
      name: 'Abbey Road',
      year: 1969,
      genre: 'Rock',
      country: 'UK'
    },
    {
      group: 'Michael Jackson',
      name: 'Thriller',
      year: '1982',
      genre: 'Rock',
      country: 'USA'
    }
  ];
  foundAlbums: Album[] = [];
  searchText: string;

  Search() {
    console.log(this.searchText);
    
    console.log(this.albums.filter(a => a.country.indexOf(this.searchText) > -1, this));
    console.log(this.foundAlbums);
    
    this.foundAlbums = this.foundAlbums.concat(this.albums.filter(a => a.country.indexOf(this.searchText) > -1, this));
    console.log(this.foundAlbums);
    
  };

}
