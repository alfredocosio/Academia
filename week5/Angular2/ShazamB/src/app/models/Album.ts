export class Album {
    group: string
    name: string
    year: number|string
    genre: string
    country: string
}