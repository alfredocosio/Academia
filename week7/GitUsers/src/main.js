import Vue from 'vue'
import App from './App.vue'
import Users from './Users.vue'
import User from './User.vue'
import Info from './Info.vue'
import Repository from './Repository.vue'
import Followers from './Followers.vue'
import Router from 'vue-router'
import store from './store'

Vue.use(Router)

var router = new Router({
  routes: [
    {
      path: '/Users',
      name: 'Users',
      component: Users
    },
    {
      path: '/Users/:username',
      name: 'User',
      component: User,
      children: [
        {
          path: '/Users/:username/Info',
          name: 'Info',
          component: Info
        },
        {
          path: '/Users/:username/Repository',
          name: 'Repository',
          component: Repository
        },
        {
          path: '/Users/:username/Followers',
          name: 'Followers',
          component: Followers
        }
      ]
    }
  ]
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
