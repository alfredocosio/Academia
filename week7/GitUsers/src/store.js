import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

const state = {
    usersResult: [],
    userProfile: {},
    userFollowers: [],
    userRepos: []
}

const mutations = {
    getUsersBySearchText(state, searchText) {
        Vue.axios.get(`https://api.github.com/search/users?q=${searchText}&page=1&per_page=10&order=score`).then((response) => {
                { state.usersResult = response.data.items };
            })
    },
    getProfileByUsername(state, username) {
        // console.log(username+" hellojon");
        Vue.axios.get(`https://api.github.com/users/${username}`).then((response) => {
            // console.log(response.data);
            { state.userProfile = response.data };
        });
    },
    getFollowersByUsername(state, username) {
        // console.log(username + " for followers")
        Vue.axios.get(`https://api.github.com/users/${username}/followers`).then((response) => {
            // console.log(response.data)
            {state.userFollowers = response.data};
        })
    },
    getReposByUsername(state, username) {
        console.log(username + " for followers")
        Vue.axios.get(`https://api.github.com/users/${username}/repos`).then((response) => {
            console.log(response.data)
            {state.userRepos = response.data};
        })
    }
}

const actions = {
    getUsers({ commit, state }, searchtext) {
        console.log(searchText)
        commit('getUsersBySearchText', searchtext);
    },
    getUser({commit, state}, username) {
        commit('getProfileByUsername', username)
    },
    getUserFollowers({commit, state}, username) {
        commit('getFollowersByUsername',username)
    },
    getUserRepos({commit, state}, username) {
        commit('getReposByUsername', username)
    }
}

export default new Vuex.Store({
    state,
    mutations,
    actions
})
